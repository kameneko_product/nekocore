package kame.nekocore.inventory;

import kame.nekocore.NekoCore;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.PluginDisableEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * ActionInventoryListener
 */
public class ActionInventoryListener implements Listener {

    /**
     * ActionInventoryListenerのシングルトンインスタンスを取得します。
     */
    public static final ActionInventoryListener instance = new ActionInventoryListener();

    private final Map<ActionHolder, ActionInventory> inventories = new HashMap<>();

    private final Map<HumanEntity, ActionHolder> players = new HashMap<>();

    private ActionInventoryListener() {

    }

    @EventHandler
    private void onActionInventoryOpen(InventoryOpenEvent event) {
        if (event.getInventory().getHolder() instanceof ActionHolder holder) {
            inventories.put(holder, holder.getActionInventory());
            players.put(event.getPlayer(), holder);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onInventoryClick(InventoryClickEvent event) {
        if (event.getInventory().getHolder() instanceof ActionHolder holder) {
            event.setCancelled(true);
            if (event.getInventory() != event.getClickedInventory()) return;
            var actionInv = inventories.get(holder);
            var itemStack = actionInv.getItem(event.getSlot());
            if (itemStack instanceof ActionItemStack item) {
                item.getClickEvents().forEach(x -> x.click(event));
            } else {
                actionInv.getClickEvents().forEach(x -> x.click(event));
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onDrag(InventoryDragEvent event) {
        if (event.getInventory().getHolder() instanceof ActionHolder holder) {
            event.setCancelled(true);
            for (var slot : event.getInventorySlots()) {
                var actionInv = inventories.get(holder);
                var itemStack = actionInv.getItem(slot);
                if (itemStack instanceof ActionItemStack item) {
                    item.getDragEvents().forEach(x -> x.drag(event));
                } else {
                    actionInv.getDragEvents().forEach(x -> x.drag(event));
                }
            }
        }
    }

    @EventHandler
    private void onInventoryClose(InventoryCloseEvent event) {
        var inv = inventories.remove(players.remove(event.getPlayer()));
        if (inv != null) {
            inv.getCloseEvents().forEach(x -> x.close(event));
        }
    }

    @EventHandler
    private void onPlayerQuit(PlayerQuitEvent event) {
        inventories.remove(players.remove(event.getPlayer()));
    }

    @EventHandler
    private void onPluginDisable(PluginDisableEvent event) {
        if (event.getPlugin() instanceof NekoCore) {
            players.keySet().forEach(HumanEntity::closeInventory);
        }
    }
}
