package kame.nekocore.inventory;

import org.bukkit.event.inventory.InventoryCloseEvent;

/**
 * インベントリを閉じたときのイベントを定義します。
 */
public interface CloseEvent {
    /**
     * 閉じたときのイベント
     *
     * @param event クローズイベント
     */
    void close(InventoryCloseEvent event);
}