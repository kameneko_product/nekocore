package kame.nekocore.inventory;

import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * クリック時のイベントを定義します。
 */
public interface ClickEvent {
    /**
     * クリック時のイベント
     *
     * @param event クリックイベント
     */
    void click(InventoryClickEvent event);
}