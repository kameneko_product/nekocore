package kame.nekocore.inventory;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.jetbrains.annotations.NotNull;

/**
 * ActionInventoryが持つInventoryHolderの実装です。
 */
public class ActionHolder implements InventoryHolder {

    private final Inventory inventory;

    private final ActionInventory actionInventory;

    /**
     * ActionInventoryオブジェクトからこのインスタンスを生成します。
     *
     * @param inv ActionInventoryオブジェクト
     */

    public ActionHolder(@NotNull ActionInventory inv) {
        this.actionInventory = inv;
        inventory = Bukkit.createInventory(this, inv.getSize(), inv.getTitle());
    }

    /**
     * インスタンス生成時に使用したActionInventoryインスタンスを返します。
     *
     * @return このインスタンスの生成に使用されたActionInventoryインスタンス
     */
    public @NotNull ActionInventory getActionInventory() {
        return actionInventory;
    }

    /**
     * このインスタンスに紐づけられているインベントリを取得します。
     *
     * @return このインスタンスに紐づけられているインベントリ情報
     */
    public @NotNull Inventory getInventory() {
        return inventory;
    }
}