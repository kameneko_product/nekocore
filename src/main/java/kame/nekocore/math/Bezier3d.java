package kame.nekocore.math;

/**
 * 3次元の3次ベジェ曲線に必要な機能を提供するレコードクラスです。
 *
 * @param x1 座標1 X値
 * @param y1 座標1 Y値
 * @param z1 座標1 Z値
 * @param x2 座標2 X値
 * @param y2 座標2 Y値
 * @param z2 座標2 Z値
 * @param x3 座標3 X値
 * @param y3 座標3 Y値
 * @param z3 座標3 Z値
 * @param x4 座標4 X値
 * @param y4 座標4 Y値
 * @param z4 座標4 Z値
 */
public record Bezier3d(double x1, double y1, double z1, double x2, double y2, double z2,
                       double x3, double y3, double z3, double x4, double y4, double z4) {

    /**
     * 2つの線分からこのインスタンスを初期化します。
     *
     * @param l1 座標1と座標2を結ぶ線
     * @param l2 座標3と座標4を結ぶ線
     */
    public Bezier3d(Line3d l1, Line3d l2) {
        this(l1.x1(), l1.y1(), l1.z1(), l1.x2(), l1.y2(), l1.z2(), l2.x1(), l2.y1(), l2.z1(), l2.x2(), l2.y2(), l2.z2());
    }

    /**
     * 4つの座標値からこのインスタンスを初期化します。
     *
     * @param p1 座標1
     * @param p2 座標2
     * @param p3 座標3
     * @param p4 座標4
     */
    public Bezier3d(Point3d p1, Point3d p2, Point3d p3, Point3d p4) {
        this(p1.x(), p1.y(), p1.z(), p2.x(), p2.y(), p2.z(), p3.x(), p3.y(), p3.z(), p4.x(), p4.y(), p4.z());
    }

    /**
     * 2つの線分から0.0～1.0の範囲でベジェ補間したときの引数[t]の位置を取得します
     *
     * @param t ベジェ補間で取得する位置の値
     * @return 取得する位置の座標値
     */
    public Point3d lerpBezier(double t) {
        var t1 = (1 - t) * (1 - t) * (1 - t);
        var t2 = (1 - t) * (1 - t) * t * 3;
        var t3 = (1 - t) * t * t * 3;
        var t4 = t * t * t;
        var x = t1 * x1 + t2 * x2 + t3 * x3 + t4 * x4;
        var y = t1 * y1 + t2 * y2 + t3 * y3 + t4 * y4;
        var z = t1 * z1 + t2 * z2 + t3 * z3 + t4 * z4;
        return new Point3d(x, y, z);
    }

}
