package kame.nekocore.math;

import org.bukkit.Location;
import org.bukkit.util.Vector;

/**
 * 3次元の直線の計算に必要な機能を提供するレコードクラスです。
 *
 * @param x1 始点 X座標
 * @param y1 始点 Y座標
 * @param z1 始点 Z座標
 * @param x2 終点 X座標
 * @param y2 終点 Y座標
 * @param z2 終点 Z座標
 */
public record Line3d(double x1, double y1, double z1, double x2, double y2, double z2) {

    /**
     * Locationオブジェクトからこのインスタンスを初期化します。
     *
     * @param loc1 始点座標
     * @param loc2 終点座標
     */
    public Line3d(Location loc1, Location loc2) {
        this(loc1.getX(), loc1.getY(), loc1.getZ(), loc2.getX(), loc2.getY(), loc2.getZ());
    }

    /**
     * Locationオブジェクトと距離からこのインスタンスを初期化します。
     *
     * @param loc      始点座標(回転含む)
     * @param distance 距離
     */
    public Line3d(Location loc, double distance) {
        this(loc.toVector(), loc.toVector().add(loc.getDirection().multiply(distance)));
    }

    /**
     * Vectorオブジェクトからこのインスタンスを初期化します。
     *
     * @param vec1 始点座標
     * @param vec2 終点座標
     */
    public Line3d(Vector vec1, Vector vec2) {
        this(vec1.getX(), vec1.getY(), vec1.getZ(), vec2.getX(), vec2.getY(), vec2.getZ());
    }

    /**
     * Pointオブジェクトからこのインスタンスを初期化します。
     *
     * @param pos1 始点座標
     * @param pos2 終点座標
     */
    public Line3d(Point3d pos1, Point3d pos2) {
        this(pos1.x(), pos1.y(), pos1.z(), pos2.x(), pos2.y(), pos2.z());
    }

    /**
     * 直線の始点から終点を0.0～1.0の範囲で線形補間したときの引数[t]の位置を取得します。
     *
     * @param t 線形補間で取得する位置の値
     * @return 取得する位置の座標値
     */
    public Point3d lerp(double t) {
        var _t = 1 - t;
        return new Point3d(x1 * _t + x2 * t, y1 * _t + y2 * t, z1 * _t + z2 * t);
    }
}
